//var submitButton = document.getElementById('bsr-submit-button');
//submitButton.onclick = getFormInfo();


function getFormInfo() {
	console.log('entered getFormInfo!');
	// call displayinfo
	var name = document.getElementById("input-text-4858239").value;
	console.log('Number you entered is ' + name);
	makeNetworkCallToCountryApi(name);

} // end of get form info


function makeNetworkCallToCountryApi(name) {
	console.log('entered make nw call' + name);
	// set up url
	var xhr = new XMLHttpRequest(); // 1 - creating request object
	var url = "https://api.nationalize.io?name=" + name;
	xhr.open("GET", url, true); // 2 - associates request attributes with xhr

	// set up onload
	xhr.onload = function(e) { // triggered when response is received
		// must be written before send
		console.log(xhr.responseText);
		// do something
		updateCountryWithResponse(name, xhr.responseText);
	}

	// set up onerror
	xhr.onerror = function(e) { // triggered when error response is received and must be before send
		console.error(xhr.statusText);
	}

	// actually make the network call
	xhr.send(null) // last step - this actually makes the request

} // end of make nw call



function updateCountryWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("net-container1");

    if(response_json['country'] == null){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  name + ', the country you are from is ' + response_json['country'][1]['country_id'];
        var country = response_json['country'][1]['country_id'];
        makeNetworkCallToAgeApi(country);
    }
} // end of updateCountryWithResponse


function makeNetworkCallToAgeApi(country){
    console.log('entered make nw call' + country);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.agify.io?name=" + country;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateAgeWithResponse(country, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateAgeWithResponse(age, response_text){
    // update a label
	
	 var response_json = JSON.parse(response_text);

    var label2 = document.getElementById("net-container2");
    label2.innerHTML = "Based on your country, your age is " + response_json["age"];

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode("Here are your results"); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateAgeWithResponse



